package com.devcamp.j04_javabasic.s10;

public class CCat extends CPet implements IRunable , Other{
	@Override
	public void run() {
		System.out.println("Meo chay");
	}
	@Override
	public void running() {
	}
	@Override
	public void other() {
		System.out.println("Cat other");
	}
	@Override
	public int other(int param) {
		return 5;
	}
	@Override
	public String other(String param) {
		return "Cat";
	}
	@Override
	public void animalSound() {
		System.out.println("Cat sound...");
	}
	// hàm khởi tạo có 2 tham số
	public CCat(String name, int age) {
		this.name = name;
		this.age = age;
	}
}
