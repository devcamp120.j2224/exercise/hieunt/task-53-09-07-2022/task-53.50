import java.util.ArrayList;
import com.devcamp.j04_javabasic.s10.*;

public class App {
    public static void main(String[] args) throws Exception {
        CPet dog = new CDog("Lulu", 3);
        CPet cat = new CCat("Miumiu", 2);

        ArrayList<CPet> petList = new ArrayList<>();
        petList.add(dog);
        petList.add(cat);

        CPerson person1 = new CPerson(1, 30, "Hieu", "Nguyen", petList);
        CPerson person2 = new CPerson();

        System.out.println(person1);
        System.out.println(person2);

        // System.out.println("person1 có id là " + person1.getId());
        // System.out.println("person1 có age là " + person1.getAge());
        // System.out.println("person1 có name là " + person1.getFirstName() + " " + person1.getLastName());
        // System.out.println("person1 có pets là " + person1.getPets());

        // System.out.println("person2 có id là " + person2.getId());
        // System.out.println("person2 có age là " + person2.getAge());
        // System.out.println("person2 có name là " + person2.getFirstName() + " " + person2.getLastName());
        // System.out.println("person2 có pets là " + person2.getPets());
    }
}
